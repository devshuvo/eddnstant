<?php
/**
 * The Layout
 */
add_action('wp_footer', 'eddnstant_layout_function');
function eddnstant_layout_function(){

    if ( !class_exists( 'Easy_Digital_Downloads' ) ) {
        return;
    }

	global $eddnstant_opt;
    ?>
    <div class="eddnstant-checkout-wrap">
    	<?php if( $eddnstant_opt['show-close-btn'] == 1 ) : ?>
            <div class="eddnstant-close-wrap">
    		    <a href="#" class="eddnstant-close" title="<?php esc_attr_e( 'Close', 'eddnstant' ); ?>"><?php eddnstant_svg_icon('close'); ?></a>
            </div>
    	<?php endif; ?>

    	<?php if( $eddnstant_opt['cart-position'] != 1 ) : ?>
	    	<a href="#" id="eddnstant-toggler" class="eddnstant-cart-header">
				<?php eddnstant_svg_icon('shopping_cart'); ?>
				<?php echo eddnstant_cart_count(); ?>
			</a>
		<?php endif; ?>

        <div class="eddnstant-checkout-outer">
            <div class="eddnstant-checkout-inner">
                <?php echo do_shortcode('[download_checkout]'); ?>
            </div>
            <span class="eddnstant-loader"><?php eddnstant_svg_icon('spinner'); ?></span>
        </div>
    </div>

	<?php if( $eddnstant_opt['cart-position'] != 0 ) : ?>
	    <div id="eddnstant-sticky-cart" class="eddnstant-sticky-cart eddnstant-cart-shake">
	    	<span class="eddnstant-icon-cart"><?php eddnstant_svg_icon('shopping_basket'); ?></span>
	    	<?php echo eddnstant_cart_count(); ?>
	    </div>
	<?php endif; ?>
    <?php
}
